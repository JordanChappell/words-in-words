import itertools

# is_word function tests if a word is English using PyDictionary


def is_word(test):
    from PyDictionary import PyDictionary

    dictionary = PyDictionary()

    try:
        output = dictionary.meaning(test)
    except:
        return False
    else:
        return bool(output)

# list_unique_perms builds a list of all unique permutations of an input

def list_unique_perms(perms):
    import sys
    import os

    # list to hold all english words contained within input
    list = []
    # silence output
    sys.stdout = open(os.devnull, "w")
    # loop through permutations and check for an english meaning
    print('Checking for words...')
    for x in perms:
        check_word = is_word(''.join(x))
        if check_word:
            list.append(''.join(x))
    # unsilence? output
    sys.stdout = sys.__stdout__
    # return filled list
    return list


# take user input
word = input("Enter a word: ")

# dictionary lookup input word
result = is_word(word)
# check that input word has a dictionary meaning
# exit program if input word is not a "word"
if not result:
    print(word, "is not an English word")
    exit(1)

# list to hold all unique permutations of word_list
unique_permutations = []

# All permutations of 'word' with equal length
# loop finds all permutations from 1 to len(word)
for i in range(len(word) - 1):
    permutations = list(itertools.permutations(word, i + 2))
    for x in permutations:
        if x not in unique_permutations:        # adds unique permutations
            if list(x) != list(word):           # test against original word
                unique_permutations.append(x)

# list to hold all english words contained within input
english_word_list = list_unique_perms(unique_permutations)

# USER OUTPUT

print('Input word: ', word)
print('All English words found in input: ')
# print all words in word
for x in english_word_list:
    print(x)
