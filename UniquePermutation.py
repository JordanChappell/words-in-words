class UniquePermutation:

    def __init__(self):
        self.permutation_list = []

    # Precondition: word must be a string
    # Postcondition: recursively builds a list of all possible permutations of input
    #                and returns
    def all_permutations(self, word):
        if len(word) <= 1:
            return word






